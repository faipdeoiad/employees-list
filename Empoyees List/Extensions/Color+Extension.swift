//
//  Color+Extension.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 01/05/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

extension Color {
    
    // Color Assets
     static let blackWhite = Color("BlackWhite")
     static let whiteBlack = Color("WhiteBlack")
}

//
//  Network.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation
import Combine

struct NetworkService {
    
    
    /// Returns a data publisher for the employee
    func employeeSearchPublisher(for employe: Employee) -> AnyPublisher<Data, URLError>? {
        guard let url = constructUrl(for: employe) else { return nil }
            
        return  URLSession.shared
                  .dataTaskPublisher(for: url)
                  .map(\.data)
                  .eraseToAnyPublisher()
    }
    
    /// Constructs 
    private func constructUrl(for employe: Employee) -> URL? {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "www.googleapis.com"
        components.path = "/customsearch/v1"
        
        components.queryItems = [
            URLQueryItem(name: "key", value: "AIzaSyDKtnroRFsrtGRGjKMuXjSmN_5sG0dK21o"),
            URLQueryItem(name: "cx", value: "013995831144969854469:h6xizbmrh9n"),
        ]
        
        components.queryItems?.append(URLQueryItem(name: "q", value: employe.fullName))
        
        return components.url
    }
}



//
//  AddEmployeeView.swift
//  Empoyees List
//
//  Created by Ziga Porenta on 29/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct AddEmployeeView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State private var showingValidationAlert = false
 
    // Employee values
    @State private var name: String = ""
    @State private var surname: String = ""
    @State private var selectedGender: Gender = .male
    @State private var salary: Double? = 1000
    // Set the date 30 years back
    @State private var birthDate = Calendar.current.date(byAdding: .year, value: -30, to: Date())!

        
    // MARK: -
    
    var body: some View {
        
        Form {
                                    
            Section(header: Text("Name")) {
                TextField("First name", text: $name)
                    .textContentType(.givenName)
                TextField("Last name", text: $surname)
                    .textContentType(.familyName)
            }
            
            Section(header: Text("Personal info")) {
                
                Picker(selection: $selectedGender, label: Text("Select a gender")) {
                    ForEach(0 ..< Gender.allCases.count) {
                        Text(Gender.allCases[$0].rawValue)
                            .tag(Gender.allCases[$0])
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.vertical)
                
                DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
                    Text("Birthday")
                }
            }
            
            Section(header: Text("Salary")) {
            
            
                TextField("Enter salary", value: $salary, formatter: NumberFormatter.currency)
                .font(.title)
                .padding()
                .multilineTextAlignment(.center)

                   
            }
            
            Section {
                Button(action: {
                    self.createEmployee()
                }) {
                    Text("Save")
                }
            }
        }
        .keyboardAdaptive()

        .alert(isPresented: $showingValidationAlert, content: {
            Alert(title: Text("Oops"), message: Text("Please enter all the required fields"), dismissButton: .default(Text("OK")))
        })
        .navigationBarTitle("Add employee")
    }
    
    private func createEmployee() {
                
        // Validate data
        guard !name.isEmpty && !surname.isEmpty  else {
            showingValidationAlert = true
            return
        }
        
        guard let salary = salary, salary > 0  else {
            showingValidationAlert = true
            return
        }
         
        // Create and save the new employee
        let newEmployee = Employee(name: name, surname: surname, birthday: birthDate, gender: selectedGender, salary: salary)
        newEmployee.save()
        
        // Pop the navigation
        presentationMode.wrappedValue.dismiss()
        
    }
}

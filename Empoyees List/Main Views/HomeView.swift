//
//  ContentView.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 29/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var viewModel: HomeViewViewModel
    
    @State private var selectedEmployee: Employee?
    @State private var showingPublicProfile = false
    
    var body: some View {
        
        NavigationView {
            VStack(spacing: 0.0) {
                
                // If there are no employees show the empty screen
                if viewModel.allEmployees.isEmpty {
                    NoDataView(
                        title: "No employees to display",
                        subtitle: "Click on the ADD button to add your first one."
                    )
                    
                }
                    // Otherwise show the List view with all the employees
                else {
                    
                    List {
                        ForEach(viewModel.allEmployees) { employee in
                            EmployeeRowView(viewModel: EmployeeViewModel(employee: employee))
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    self.selectedEmployee = employee
                                    self.showingPublicProfile = true
                            }
                        }
                    }
                    .listStyle(GroupedListStyle())
                }
                
                // Footer
                
                // Separator
                Rectangle()
                    .frame(height: 1)
                    .opacity(0.25)
                    .padding(.bottom)
                
                // Button Stack
                HStack {
                    Spacer()
                    
                    NavigationLink(destination: AddEmployeeView()) {
                        FooterButton(title: "ADD", image:  Image(systemName: "person.crop.circle"))
                    }
                    .frame(height: 60)

                    Spacer()
                    
                    NavigationLink(destination: StatisticView(viewModel: StatisticsViewModel(employees: viewModel.allEmployees))) {
                        FooterButton(title: "ANALYTICS", image: Image(systemName: "chart.pie"))
                    }
                    .frame(height: 60)
 
                    Spacer()
                }
                .padding(.bottom)
                .foregroundColor(Color.blackWhite)
            }
            .sheet(isPresented: $showingPublicProfile) {
                PublicProfileView(viewModel: PublicProfileViewModel(employe: self.selectedEmployee!))
            }
                
            .navigationBarTitle(Text("Employees"))
            .onAppear {
                self.viewModel.refresh()
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
    
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        
        Group {
            HomeView(viewModel: HomeViewViewModel())
               .environment(\.colorScheme, .light)
            
            HomeView(viewModel: HomeViewViewModel())
               .environment(\.colorScheme, .dark)
        }
    }
}

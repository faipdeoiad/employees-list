//
//  PublicProfileView.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct PublicProfileView: View {
    
    @ObservedObject var viewModel: PublicProfileViewModel
    
    @State var showingSafari = false
    @State var urlString = ""
    
    var body: some View {
        VStack {
            VStack {
                Text(viewModel.employee.fullName).font(.largeTitle).bold()
                Text("Public profile")
            }
            .padding()
            
            Spacer()
            
            // Show activity indicator until we get back the search hits
            if viewModel.searchItems.isEmpty {
                ActivityIndicator(shouldAnimate: .constant(true))
                    .foregroundColor(.red)
                Spacer()
                
            } else {
                
                List {
                    ForEach(viewModel.searchItems) { item in
                        SearchHitRowView(searchHit: item)
                            .onTapGesture {
                                self.urlString = item.link ?? ""
                                self.showingSafari = true
                        }
                    }
                }
                .listStyle(GroupedListStyle())
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .sheet(isPresented: $showingSafari) {
            SafariView(url: URL(string: self.urlString)!)
        }
    }
}

struct PublicProfileView_Previews: PreviewProvider {
    static var previews: some View {
        PublicProfileView(viewModel: PublicProfileViewModel(employe: Employee.mock))
    }
}

//
//  StatisticView.swift
//  Empoyees List
//
//  Created by Ziga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct StatisticView: View {
    
    let viewModel: StatisticsViewModel
    
    var body: some View {
        
        VStack(spacing: 0.0) {
            
            if viewModel.noDataToDisplay {
                NoDataView(
                    title: "No data to display",
                    subtitle: "Add some employees to see the analytics."
                )
            } else {
                
                Rectangle()
                    .frame(height:1)
                    .opacity(0.15)
                    .padding(.horizontal)

                ScrollView(.vertical, showsIndicators: false) {
                    
                   
                    VStack(alignment: .leading, spacing: 28) {
                        
                        StatisticItem(title: "Average age:", value: viewModel.averageAgeString)
                    
                        StatisticItem(title: "Median age:", value: viewModel.medianAgeString)
                        
                        StatisticItem(title: "Max salary:", value: viewModel.maxSalaryString)
                        
                        StatisticItem(title: "Gender distribution:", value: viewModel.genderRatioString)
                    }
                    .padding()
                }
                .background(Color.whiteBlack)
                .edgesIgnoringSafeArea(.vertical)
                .navigationBarTitle(Text("Analytics"))
            }
        }
    }
}

struct StatisticItem: View {
    
    let title: String
    let value: String
    var body: some View {
        
        VStack(alignment: .leading, spacing: 5.0) {
            VStack(alignment: .leading) {
                Text(title.uppercased())
                    .font(.caption)
                    .kerning(1.0)
                Text(value)
                    .font(.title).bold()
            }
            Rectangle()
                .frame(height:1)
                .opacity(0.15)
        }.padding(.horizontal)
    }
}

struct StatisticView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticView(viewModel: StatisticsViewModel.mock)
    }
}

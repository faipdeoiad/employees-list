//
//  EmployeeRowView.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct EmployeeRowView: View {
    
    let viewModel: EmployeeViewModel
    var body: some View {
        
        HStack {
            
            viewModel.profileImage
                .padding(.trailing)
            
            VStack(alignment: .leading) {
                Text(viewModel.fullName).bold()
                Text(viewModel.birthDayString)
                    .font(.footnote)
            }
            Spacer()
            Text(viewModel.salaryString)
  
        }.padding()

    }
}

struct EmployeeRowView_Previews: PreviewProvider {
    static var previews: some View {
        EmployeeRowView(viewModel: EmployeeViewModel(employee: Employee.mock))
    }
}

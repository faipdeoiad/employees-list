//
//  FooterButton.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 01/05/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct FooterButton: View {
    
    let title: String
    let image: Image
    
    var body: some View {
        VStack {
            image
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            
            Text(title)
                .bold()
                .font(.footnote)
        }
    }
}

struct FooterButton_Previews: PreviewProvider {
    static var previews: some View {
        FooterButton(title: "Button", image: Image(systemName: "person"))
    }
}

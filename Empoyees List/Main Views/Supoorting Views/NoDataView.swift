//
//  NoDataView.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 01/05/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct NoDataView: View {
    
    let title: String
    let subtitle: String
    var body: some View {
        
        VStack {
            Spacer()
            VStack(alignment: .leading, spacing: 10) {
                Text(title)
                    .font(.headline)
                Text(subtitle)
                    .font(.subheadline)
            }
            Spacer()
        }
        
    }
}

struct NoDataView_Previews: PreviewProvider {
    static var previews: some View {
        NoDataView(title: "No employees to display", subtitle: "Click on the ADD button to add your first one.")
    }
}

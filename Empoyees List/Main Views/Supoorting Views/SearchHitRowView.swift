//
//  SearchHitRowView.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import SwiftUI

struct SearchHitRowView: View {
    
    let searchHit: SearchHit 
    var body: some View {
        
        VStack(alignment: .leading, spacing: 8.0) {
            Text(searchHit.title ?? "-")
                .bold()
            
            Text(searchHit.link ?? "-")
                .italic()
            
            Text(searchHit.cleanSnippet)
        }
        .padding()
    }
}

struct SearchHitRowView_Previews: PreviewProvider {
    static var previews: some View {
        SearchHitRowView(searchHit: SearchHit.mock)
    }
}

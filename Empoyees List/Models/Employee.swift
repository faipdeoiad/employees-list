//
//  Employee.swift
//  Empoyees List
//
//  Created by Ziga Porenta on 29/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation

class Employee: Codable, Identifiable {
   
    let id = UUID()

    let name: String
    let surname: String
    let birthday: Date
    let salary: Double
    let gender: Gender
    
    var age: Int {
        let components = Calendar.current.dateComponents([.year], from: birthday, to: Date())
        return components.year ?? 0
    }
    
    var fullName: String {
        [name, surname].joined(separator: " ")
    }
    
    init(name: String, surname: String, birthday: Date, gender: Gender, salary: Double) {
        self.name = name
        self.surname = surname
        self.birthday = birthday
        self.gender = gender
        self.salary = salary
    }
    
    
    func save() {
        var allEmployees = Storage.retrieve(Storage.employeesDirectory, from: .documents, as: [Employee].self) ?? []
        allEmployees.append(self)
    
        Storage.store(allEmployees, to: .documents, as: Storage.employeesDirectory)
    }
    
    // MARK: - Mock
    // Mock employee to be used for swiftUI previews
    static var mock: Employee {
        return Employee(name: "John", surname: "Appleseed", birthday: Date().addingTimeInterval(-60*60*24*365*30), gender: .male, salary: 2000)
    }
}

//
//  Gender.swift
//  Empoyees List
//
//  Created by Ziga Porenta on 29/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation


enum Gender: String, Codable, CaseIterable {
    case male = "Male"
    case female = "Female"
}

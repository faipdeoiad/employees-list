//
//  SearchItem.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation

struct SearchHit: Codable, Identifiable {
    let title: String?
    let link: String?
    let snippet: String?
    let id = UUID()
    
    // For some reason the API returns snippets with randomly inserted new lines
    var cleanSnippet: String {
        snippet?.replacingOccurrences(of: "\n", with: "") ?? ""
    }
    
    // Mock for SwiftUI previews
    static let mock =  SearchHit(title: "Apple", link: "https://www.apple.com/", snippet: "Discover the innovative world of Apple and shop everything iPhone, iPad, Apple \nWatch, Mac, and Apple TV, plus explore accessories, entertainment, and expert ...")
}

//
//  EmployeeViewModel.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation
import SwiftUI

struct EmployeeViewModel {
    
    let employee: Employee
    
    // MARK: - Computed values
    
    var birthDayString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: employee.birthday)
    }
    
    var fullName: String {
        employee.fullName
    }
    
    var salaryString: String {
        NumberFormatter.currency.string(from: NSNumber(value: employee.salary)) ?? "-"
    }
    
    var profileImage: AnyView {
        switch employee.gender {
        case .male: return AnyView(Image(systemName: "person.circle")
            .foregroundColor(Color.blue))
        case .female: return AnyView(Image(systemName: "person.circle")
            .foregroundColor(Color.pink))
        }
    }
}

//
//  HomeViewViewModel.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation

class HomeViewViewModel: ObservableObject {
    
    @Published var allEmployees: [Employee] = Storage.retrieve(Storage.employeesDirectory, from: .documents, as: [Employee].self) ?? []
    
    func refresh() {
        allEmployees = Storage.retrieve(Storage.employeesDirectory, from: .documents, as: [Employee].self) ?? []
    }
    
}


//
//  PublicProfileViewModel.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation
import Combine

class PublicProfileViewModel: ObservableObject {
    
    let employee: Employee
    let service = NetworkService()
    
    init(employe: Employee) {
        self.employee = employe
        fetchData()
    }
    
    private var subscriptions = Set<AnyCancellable>()

    @Published var searchItems = [SearchHit]()
    
    func fetchData() {
        
        guard let publisher = service.employeeSearchPublisher(for: employee) else { return }
        
        publisher
            .retry(2)
            .receive(on: DispatchQueue.main)
            .compactMap { self.parseSearchItems(data: $0)}
            .decode(type: [SearchHit].self, decoder: JSONDecoder())
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    print("ALL DONE")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }) { items in
                self.searchItems = Array(items.prefix(5))
        }
        .store(in: &subscriptions)
    }
    
    
    private func parseSearchItems(data: Data) -> Data? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            guard let completeDic = json?["items"] as? NSArray else { return nil }
            return try? Data(JSONSerialization.data(withJSONObject: completeDic, options: .fragmentsAllowed))
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}

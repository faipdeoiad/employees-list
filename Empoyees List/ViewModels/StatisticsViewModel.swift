//
//  StatisticViewModel.swift
//  Empoyees List
//
//  Created by Žiga Porenta on 30/04/2020.
//  Copyright © 2020 blue42. All rights reserved.
//

import Foundation

struct StatisticsViewModel {
    
    let employees: [Employee]
    
    
    // MARK: - Computed values
    
    var noDataToDisplay: Bool {
          employees.isEmpty
    }
  
    private var averageAge: Double {
        employees.map { Double($0.age) }.reduce(0, +) / Double(employees.count)
    }
    
    private var medianAge: Double {
         employees.map { $0.age }.median()
    }
    
    private var maxSalary: Double {
       employees.map { $0.salary }.max() ?? 0.0
    }
    
    private var genderRatio:(male: Double, female: Double) {
        
        let totalCount = Double(employees.count)
        let maleCount = Double(employees.filter {$0.gender == .male}.count)
        let femaleCount = Double(employees.filter { $0.gender == .female}.count)
        
        let malePercent = maleCount*100.0/totalCount
        let femalePercent = femaleCount*100.0/totalCount
        
        return(malePercent, femalePercent)
    }
    
    // MARK: - Public strings
    
    var averageAgeString: String {
        String(format: "%.01f years", averageAge)
    }

    var medianAgeString: String {
        String(format: "%.01f years", medianAge)
    }
    
    var maxSalaryString: String {
        NumberFormatter.currency.string(from: NSNumber(value: maxSalary)) ?? "-"
    }
    
    var genderRatioString: String {
        String(format: "%.01f%% male\n%.01f%% female", genderRatio.male, genderRatio.female)
    }
    
    
    // Mock to be used with SwiftUI previews
    static let mock = StatisticsViewModel(employees: [
        Employee.mock,
        Employee(name: "Jane", surname: "Doe", birthday: Date().addingTimeInterval(-60*60*24*365*27), gender: .female, salary: 2400)
    ])
}


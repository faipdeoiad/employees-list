#  Readme

Employees List is a simple app for managing employee contacts.
The app is compatible with all iPhones and iPads running iOS 13.4.
It supports dark and light mode, dynamic text and split views.

The app is made with SwiftUI and Combine, using Swift 5.2 and Xcode 11.4.1

The app uses Google's custom search API to get the top hits for the employee's name. 

Please note that using other versions of Xcode may produce some bugs as SwiftUI is still very much a fresh framework.  For best results run the app on an actual device.

## Installation

Simply clone or download the repository. 
Then open Empoyees List.xcodeproj.
The app does not use any 3rd party libraries so there is no need to install and pods or packages.


## Usage

You can see the list of employees on the app's home screen.

To add a new employee click the "ADD" button in the bottom toolbar and fill out the form. Required fields are "First name" and "Last name".  
Clicking on save will save the new employee.

Clicking on an employee brings up a modal sheet of top 5 hits for the employee name from google search. 
Clicking on a link opens it in a Safary browser.

To see some analytics of the current employees click the "ANALYTICS" button in the bottom toolbar.
